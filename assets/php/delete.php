<?php
require "db_conf_pdo.inc.php";
//ini_set('display_errors', 'On');

if (!isset($_POST['w_auth'])){
    echo "Access forbidden!";
    exit;
} else {
    $w_auth=$_POST['w_auth'];
}

if ($w_auth != $auth_string){
    echo "Wrong auth!";
    exit;
}

if (isset($_POST['c_options'])){
    $c_options=$_POST['c_options'];
}

if (isset($_POST['w_spec'])){
    $w_spec=$_POST['w_spec'];
}

if (isset($_POST['w_spec_all'])){
    $w_spec_all=$_POST['w_spec_all'];
}

if (isset($_POST['w_spec_date'])){
    $w_spec_date=$_POST['w_spec_date'];
}

if (isset($_POST['w_number'])){
    $w_number=$_POST['w_number'];
}

switch ($c_options) {
    case "number_of_one_word":
        if ($w_spec !=""){
            $sql = "DELETE FROM words WHERE word =:w_spec LIMIT :w_number";
            $db = db_connect_pdo();
            $q = $db->prepare($sql);
            $q->bindParam(':w_spec', $w_spec);
            $q->bindParam(':w_number', $w_number, PDO::PARAM_INT);
            $q->execute();
            if ( ! $q->rowCount() ){
              $count = 0;
              echo 'Deletion of "'.$w_spec.'" failed.'."\n";
            } else {
              $count = $q->rowCount();
              echo 'Deletion of "'.$w_spec.'" success.'."\n";
            }
        } else {
            echo "No word was specified, nothing deleted!";
        }
        break;

    case "all_of_one_word":
        if ($w_spec_all !=""){
            $sql = "DELETE FROM words WHERE word =:w_spec_all";
            $db = db_connect_pdo();
            $q = $db->prepare($sql);
            $q->bindParam(':w_spec_all', $w_spec_all);
            $q->execute();
            if ( ! $q->rowCount() ){
              $count = 0;
              echo 'Deletion of "'.$w_spec_all.'" failed.'."\n";
            } else {
              $count = $q->rowCount();
              echo 'Deletion of "'.$w_spec_all.'" success.'."\n";
            }
        } else {
            echo "No word was specified, nothing deleted!";
        }
        break;

    case "all_older_then":
        if ($w_spec_date !=""){
            $sql = "DELETE FROM words WHERE word_date_time <=:w_spec_date";
            $db = db_connect_pdo();
            $q = $db->prepare($sql);
            $q->bindParam(':w_spec_date', $w_spec_date);
            $q->execute();
            if ( ! $q->rowCount() ){
              echo 'Deletion older then "'.$w_spec_date.'" failed.'."\n";
              $count = 0;
            } else {
              echo 'Deletion older then "'.$w_spec_date.'" success.'."\n";
              $count = $q->rowCount();
            }
        } else {
            echo "No date was specified, nothing deleted!";
        }
        break;

    case "all_words":
        $sql = "DELETE FROM words";
        $db = db_connect_pdo();
        $q = $db->prepare($sql);
        $q->execute();
        if ( ! $q->rowCount() ){
          $count = 0;
          echo "Deletion of all words failed. \n";
        } else {
          $count = $q->rowCount();
          echo "Deletion of all words success. \n";
        }
        break;
        }

if ($count !== false){
    echo "Affected rows: ". $count; // Shows the number of affected rows
} else {
    echo "Error while deleting...";
}

$count = null;
?>
