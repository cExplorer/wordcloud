<?php
require "db_conf_pdo.inc.php";
require "stopwords.php";
//ini_set('display_errors', 'On');
$response = "";
$word_new = $_POST['word'];
$word_highest = "";
$min_time_between_insert = 15;
$max_number_word_highest = 20;
$min_lenght_word = 4;
$conditions = 0;
$debug = 0;

if (isset($_POST['debugmode'])){
  if ($_POST['debugmode'] == "true"){
    $debug = 1;
  }
}

// check for lock string
$sql = "SELECT word FROM words WHERE word=:w_lock";
$db = db_connect_pdo();
$q = $db->prepare($sql);
$q->bindParam(':w_lock', $lock_string);
$q->execute();

// if the blocking string is found, no further processing is necessary
// input is permitted
if ( ! $q->rowCount() ){
  $response = $response." No lock_string</br>";
} else {
  $response = $response." Got lock_string</br>input is permitted!";
  if ($debug == 1){
      echo $response;
  }
  return;
}

// forbidden input
$word_new = stripslashes($word_new);
$word_new = strip_tags($word_new);

// check for stopwords
if (in_array($word_new, $stopWords)) {
    $response = $response." Got Stopword</br>";
} else {
    $conditions += 1;
}

// check for word lenght
if ( strlen($word_new) < $min_lenght_word) {
    $response = $response." Got to short word</br>";
} else {
    $conditions += 1;
}

if ($debug == 1){
    echo $response;
}

if ($conditions <2 ){
    return;
}

$db = db_connect_pdo();
$sql = "SELECT word FROM words ORDER BY word";

// which word has the high score
if ($res = $db->query($sql)) {
    $number_a = 0;
    $high_score = 0;
    $number_a_old = 0;
    $word_old = "";

    foreach ($db->query($sql) as $row) {
        if ($row['word'] == $word_old){
            $number_a += 1;
            if ($number_a >= $number_a_old){
                $number_a_old = $number_a;
                $word_highest = $row['word'];
                $high_score = $number_a;
            }
        } else {
            $number_a = 1;
        }
        $word_old = $row['word'];
    }
    $response = $response." high score: ".strval($high_score)." ".$word_highest."</br>";
}

if ($debug == 1){
    echo $response;
}

// which word was the last inserted
$sql = $db->prepare("SELECT word, word_date_time FROM words ORDER BY word_date_time DESC");
$sql->execute();
$res = $sql->fetch(PDO::FETCH_ASSOC);
$word_last = $res['word'];
$date_time_last = $res['word_date_time'];
$response = $response. "word/date: ".$word_last."/ ".$date_time_last."</br>";

// calc timelapse for same word
$time_now = time();
$time_next = strtotime($date_time_last) + ($min_time_between_insert);
$time_diff = $time_now - $time_next;
$response = $response. "time now/ next ".$time_now."/ ".$time_next."</br>";

// check time of multiple attempts to insert the same word
// proceed only, if the new word alraedy exist AND time lapse is big enough
if ($word_last == $word_new){
    if ($time_now > $time_next){
        $conditions += 1;
    } else {
        $response = $response. "insert time space of same word to short</br>";
    }
} else {
    $conditions += 1;
}

if ($debug == 1){
    echo $response;
}

// proceed only, if the high_score < $max_number_word_highest
if ($high_score < $max_number_word_highest) {
    $conditions += 1;
} else {
    $response = $response." reached high score</br>";
}

// proceed only, if the new word has not alraedy the high score
if ($word_highest != $word_new) {
    $conditions += 1;
} else {
    $response = $response." no new word\n";
}

if ($conditions == 5 ){
    $sql = "INSERT INTO words (word, word_date_time) VALUES(?,NOW())";
    $q = $db->prepare($sql);
    $q->execute(array($word_new));
    $insert_id = $db->lastInsertId();
    $response = $response." inserted: $word_new"."</br>";
    //echo $insert_id;
} else {
    $response = $response." nothing inserted";
}

if ($debug == 1){
    echo $response;
}

?>
