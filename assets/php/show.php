<?php
require "db_conf_pdo.inc.php";
ini_set('display_errors', 'On');

//if (!isset($_POST['w_auth'])){
//    echo "Access forbidden!";
//    exit;
//} else {
//    $w_auth=$_POST['w_auth'];
//}

//if ($w_auth != $auth_string){
//    echo "No auth! Access forbidden!";
//    exit;
//}


if (isset($_POST['c_options'])){
    $c_options=$_POST['c_options'];
}

// count the number of rows
$sql = "SELECT COUNT(word) FROM words";
$db = db_connect_pdo();
$q = $db->prepare($sql);
$q->execute();
$count = $q->fetchColumn();

switch ($c_options) {
    case "number":
        if ($count !== false){
            echo "<h3>".$count."</h3>";
        } else {
            echo "No word was found, nothing to show!";
        }
        break;

    case "words":
        if ($count > 0){
            $sql = "SELECT word FROM words ORDER BY word";
            $q = $db->prepare($sql);
            $q->execute();
            $words = "";
            foreach ($q as $row) {
                $words = $words. $row['word'] . "<br>";
            }
            echo $words;
        } else {
            echo "No words found!";
        }
        break;

    case "words_single":
        if ($count > 0){
            $sql = "SELECT word, COUNT(word) FROM words GROUP BY word HAVING COUNT(word) = 1 ORDER BY word";
            $q = $db->prepare($sql);
            $q->execute();
            $words = "";
            foreach ($q as $row) {
                $words = $words. $row['word'] . "<br>";
            }
            echo $words;
        } else {
            echo "No words found!";
        }
        break;

    case "words_multiple":
        if ($count > 0){
            $sql = "SELECT word, COUNT(word) FROM words GROUP BY word HAVING COUNT(word) > 1 ORDER BY COUNT(word)";
            $q = $db->prepare($sql);
            $q->execute();
            $words = "No multiple words";
            foreach ($q as $row) {
                $words = $words. $row['COUNT(word)'] . " - " . $row['word'] . "<br>";
            }
            echo $words;
        } else {
            echo "No words found!";
        }
        break;

    case "words_time":
        if ($count > 0){
            $sql = "SELECT word, word_date_time FROM words ORDER BY word_date_time desc";
            $q = $db->prepare($sql);
            $q->execute();
            $words = "";
            foreach ($q as $row) {
                $words = $words. $row['word_date_time'] . " - " . $row['word'] . "<br>";
            }
            echo $words;
        } else {
            echo "No words found!";
        }
        break;
}
$res = null;
$db = null;
?>
